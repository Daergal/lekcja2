#!/usr/bin/env python


__version__ = '1.4'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"


class Flower: pass


rose = Flower()


class Perfume: pass


hugo_boss = Perfume()


class Cake: pass


donuts = Cake()

print(rose, hugo_boss, donuts)
